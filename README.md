Must be compassionate. Must have caring attitude towards patients.
Ability to work at odd hours.
Stamina and will to work for long hours without taking any break.
Must have patience. Must be adept at dealing with patients belonging to different age groups (children, adults and aged folks).
Good communication skills. Must be good at interdepartmental coordination and patient education.
Must be disciplined. Must be focused at work.
Must have sound nursing knowledge and skills.
Must have an eye for detail.
Willingness to keep oneself up-to-date with the developments occurring in the world of nursing.
Must have honesty and integrity. Must stand up for the rights of patients.
https://www.sharda.ac.in/course/b-sc-nursing-58
